Obrigado pelo seu pedido de suporte! Estamos rastreando sua solicitação como tíquete nº %{ISSUE_ID} e responderemos assim que possível.

Após o seu suporte ser concluído, ele poderá ficar publico em nosso [gitlab](https://gitlab.com/manjariando/web/service-desk/-/issues/?state=closed) para ajudar na duvida de outras pessoas. Caso não queira que ele se torne publico, informe agora, se você informar só após obter o suporte, nós iremos tornar publico caso acharmos necessário.
